import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { FileService } from "../services/file.service";

interface NodeField  {childrens: [], label: null}; 


@Component({
  selector: 'app-file-display',
  templateUrl: 'file.component.html'
})
export class FileComponent implements OnInit, OnDestroy{
  private fileName: string = 'zadanie_rekrutacyjne.csv';

  private fileReadedSubscription = new Subscription();
  private skipHeader = true;


  public fileContent: string = '';
  public fileContentArray: any = [] ;
  public fileContentComplex: NodeField = {childrens: [], label: null};
  public test : NodeField = {childrens: [], label: null};
  public list: any = {} ;
  


  // constructor(private fileService: FileService){};
  constructor(private fileService: FileService){};

  ngOnInit(){

    this.fileService.getFile(this.fileName);

    this.fileReadedSubscription = this.fileService.getFileReaded().subscribe(
      (fileContent: string) => {
        this.fileContent = fileContent;
        const temp_array = this.fileContent.split("\n");
        for ( let i=0; i < temp_array.length; i++ ){
          if(this.skipHeader === true && i == 0 ) continue;
          let row = (temp_array[i]+'').replace('\r', '');
          if(temp_array[i]+'' === '') continue;
          this.fileContentArray[i] = row.split(';');
        }
        console.log(this.fileContentArray);

        
        const temp_array_2 = this.fileContent.split("\n");
        
        for ( let i=0; i < temp_array_2.length; i++ ){
          if(this.skipHeader === true && i == 0 ) continue;
          let row_string = (temp_array[i]+'').replace('\r', '');
          if(temp_array[i]+'' === '') continue;

          let row_array = row_string.split(';');

          for(let j = 0; j < row_array.length; j++){
            if (row_array[j] === '') continue;

            this.getDescendantProp(this.fileContentComplex, row_array[j]);
          }
        }

        this.fileContentComplex.childrens.length;

        console.log(this.fileContentComplex.childrens.length);
        console.log(this.fileContentComplex);

        // this.test = {...this.fileContentComplex};
        this.test = JSON.parse(JSON.stringify(this.fileContentComplex));
        console.log(this.test.childrens);
        console.log(this.test.childrens.length);
        
        
      }
    );
  }

  getfileContentComplex(){
    return this.fileContentComplex;
  }

  // getDescendantProp_old(obj: any, desc: string, value: any) {
  //   var arr: any = desc.split('.');
  //   let dumy_values: any = [];
  //   while (arr.length) {
  //     let property = arr.shift();
  //     if(typeof(obj[property]) === 'undefined') obj[property] = {}
  //     obj = obj[property];
  //     dumy_values.push(property);
  //     obj.value = dumy_values.join('.');
  //   }
  //   return obj;
  // }



  getDescendantProp(obj: any, desc: string) {
    var arr: any = desc.split('.');
    let dumy_labels: any = [];

    while (arr.length) {
      let property = arr.shift();
      if(typeof(obj.childrens[property]) === 'undefined') {
        
        obj.childrens[property] = {childrens: [], label: null};
        // obj = obj.childrens[property];
      } 
      else{
        // obj = obj.childrens[property];
      }

      obj = obj.childrens[property];
      dumy_labels.push(property);
      obj.label = dumy_labels.join('.');

    }
    // return obj;
  }



  ngOnDestroy(){
    this.fileReadedSubscription.unsubscribe();
  }
}