import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from "rxjs";

@Injectable({providedIn: 'root'})
export class FileService{
  private host = 'assets/';
  // private host = 'http://127.0.0.1:4200/src/app/assets/';

  private fileReaded = new Subject<string>();

  constructor(private http: HttpClient){}

  getFile(file: string){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'text/plain, */*',
        'Content-Type': 'application/json' // We send JSON
      }),
      responseType: 'text' as 'json'  // We accept plain text as response.
    };
    this.http.get<{fileContent: string}>(this.host + file, httpOptions).subscribe(
      (fileContent) => {
        console.log(fileContent);
        this.fileReaded.next(fileContent+'');
      }
    );
  }

  getFileReaded() {
    return this.fileReaded.asObservable();
  }
}